module.exports = {
  purge: [],
  theme: {
    extend: {
      colors: {
        primary: '#DA4167',
        secondary: {
          100: '#ded4c3',
          200: '#d5c8b2',
          300: '#ccbca2',
          400: '#c3b091',
          500: '#baa481',
        }
      },
      fontFamily: {
        body: ['Nunito']
      }
    },
  },
  variants: {},
  plugins: [],
}
